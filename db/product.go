package db

import "time"

type Product struct {
	ID    uint `gorm:"primary_key"`
	Code  string
	Price uint `gorm:"not null;default:0"`
}

type ProductData struct {
	CreatedAt time.Time  `json:"-"`
	UpdatedAt time.Time  `json:"-"`
	DeletedAt *time.Time `sql:"index" json:"-"`
	Product
}

func (ProductData) TableName() string {
	return "product"
}

type ProductDB interface {
	GetProductById(string) ProductData
	GetProducts() []ProductData
}

func (db *DB) CreateTestProducts() {
	db.Create(&ProductData{Product: Product{Code: "L1", Price: 1000}})
	db.Create(&ProductData{Product: Product{Code: "L2"}})
	db.Create(&ProductData{Product: Product{Code: "L3", Price: 100}})
}

func (db *DB) GetProductById(id string) (product ProductData) {
	db.Where("id = ?", id).First(&product)
	return
}

func (db *DB) GetProducts() (products []ProductData) {
	db.Where("price > 0").Find(&products)
	return
}
