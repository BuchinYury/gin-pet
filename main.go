package main

import (
	"ginPet/db"
	"ginPet/server"
	"github.com/gin-gonic/gin"
)

func main() {
	db := db.New()
	db.Init()
	//db.CreateTestProducts()

	router := gin.Default()

	server := server.New(":8080", db, router)
	server.Run()
}
