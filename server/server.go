package server

import (
	"ginPet/db"
	"github.com/gin-gonic/gin"
	"log"
)

type Server struct {
	addr   string
	db     *db.DB
	router *gin.Engine
}

func New(addr string, db *db.DB, router *gin.Engine) *Server {
	return &Server{
		addr:   addr,
		db:     db,
		router: router,
	}
}

func (server *Server) Run() {
	initV1(server.router, server.db)

	err := server.router.Run(server.addr)
	if err != nil {
		log.Fatal(err)
	}
}
