package server

import (
	"ginPet/db"
	"github.com/gin-gonic/gin"
	"net/http"
)

func initV1(rootRouter *gin.Engine, db *db.DB) {
	v1 := rootRouter.Group("/v1")
	initV1Products(v1, db)
	initV1Test(v1)
}

func initV1Products(v1 *gin.RouterGroup, productDB db.ProductDB) {
	v1.GET("/products", getProducts(productDB))
	v1.GET("/product/:id", getProductById(productDB))
}

func getProducts(productDB db.ProductDB) gin.HandlerFunc {
	return func(c *gin.Context) {
		products := productDB.GetProducts()

		productsResponse := make([]db.Product, 0, len(products))
		for _, product := range products {
			productsResponse = append(productsResponse, product.Product)
		}

		c.JSON(http.StatusOK, productsResponse)
	}
}

func getProductById(productDB db.ProductDB) gin.HandlerFunc {
	return func(c *gin.Context) {
		id := c.Param("id")
		product := productDB.GetProductById(id)
		if product.ID == 0 {
			c.JSON(http.StatusNotFound, gin.H{
				"message": "Not found product with id - " + id,
			})
			return
		}
		c.JSON(http.StatusOK, product.Product)
	}
}

func initV1Test(v1 *gin.RouterGroup) {
	test := v1.Group("/test")
	test.GET("/:1", func(c *gin.Context) {
		c.String(http.StatusOK, "Hello /:1")
	})
	test.GET("/:1/:2", func(c *gin.Context) {
		c.String(http.StatusOK, "Hello /:1/:2")
	})
	test.GET("/:1/:2/*3", func(c *gin.Context) {
		c.String(http.StatusOK, "Hello /:1/:2/*3")
	})
}
